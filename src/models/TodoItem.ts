import { Document, model, Model, Schema } from "mongoose";

export interface ITodoItem extends Document {
  title: string,
  content: string,
  isDone: boolean,
  isPin: boolean,
}

const TodoItemSchema: Schema = new Schema(
  {
  title: String,
  content: String,
  isDone: {
    type: Boolean,
    default: false,
  },
  isPin: {
    type: Boolean,
    default: false,
  }
  },
  { timestamps: true }
);

export const TodoItemModel: Model<ITodoItem> = model<ITodoItem>('TodoItem', TodoItemSchema)