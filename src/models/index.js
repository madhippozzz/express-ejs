"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var db_config_1 = __importDefault(require("../config/db.config"));
var mongoose_1 = __importDefault(require("mongoose"));
var TodoItem_1 = require("./TodoItem");
mongoose_1.default.Promise = global.Promise;
exports.default = {
    mongoose: mongoose_1.default,
    url: db_config_1.default.url,
    todoItem: TodoItem_1.TodoItemModel,
};
