"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodoItemModel = void 0;
var mongoose_1 = require("mongoose");
var TodoItemSchema = new mongoose_1.Schema({
    title: String,
    content: String,
    isDone: {
        type: Boolean,
        default: false,
    },
    isPin: {
        type: Boolean,
        default: false,
    }
}, { timestamps: true });
exports.TodoItemModel = mongoose_1.model('TodoItem', TodoItemSchema);
