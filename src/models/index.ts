import dbConfig from '../config/db.config';
import mongoose from 'mongoose';
import { TodoItemModel } from './TodoItem';

mongoose.Promise = global.Promise;

export default {
  mongoose,
  url: dbConfig.url,
  todoItem: TodoItemModel,
};