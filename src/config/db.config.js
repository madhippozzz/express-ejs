"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MONGO_CONNECTION_URL = "mongodb://localhost:27017/nevernotes";
exports.default = {
    url: MONGO_CONNECTION_URL,
};
