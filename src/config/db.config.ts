const MONGO_CONNECTION_URL: string = "mongodb://localhost:27017/nevernotes"

export default {
  url: MONGO_CONNECTION_URL,
};