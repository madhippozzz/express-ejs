import { Request, Response, NextFunction } from 'express';
import { Router, IRouter } from 'express';
import TodoItemController from '../controllers/TodoItem.controller';
const router: IRouter = Router();

router.get('/', async function(req: Request, res: Response, next: NextFunction) {
  const data = await TodoItemController.findAll();
  res.render('index', { username: 'Phong Tran', data });
});

export default router;
