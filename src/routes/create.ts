import { Request, Response, NextFunction } from 'express';
import { Router, IRouter } from 'express';
import TodoItemController from '../controllers/TodoItem.controller';
const router: IRouter = Router();

router.get('/', async function (req: Request, res: Response, next: NextFunction) {
  res.render('create');
});

router.post('/', async function (req: Request, res: Response, next: NextFunction) {
  TodoItemController.create(req, res)
});

export default router;
