import { SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS } from 'constants';
import { Request, Response } from 'express';
import { Model } from 'mongoose';
import db from '../models';
import { ITodoItem } from '../models/TodoItem';
const TodoItem: Model<ITodoItem> = db.todoItem;

function isEmpty(o: Object) {
  for (let key in o) {
      if (Object.prototype.hasOwnProperty.call(o, key)) {
          return false;
      }
  }
  return true;
}

class TodoItemController {
  public async create(req: Request, res: Response) {
    const todoItem: ITodoItem = new TodoItem({
      title: req.body.title,
      content: req.body.content,
      isPin: req.body.isPin,
    });

    await todoItem.save((err) => {
      if (err) {
        res.status(400).send({ status: 'ERROR', message: err });
        return;
      }
      res.redirect('/');
    })
  }

  public async findAll() {
    let resData: any = [];
    await TodoItem.find().then((data) => {
      if (isEmpty(data)) {
        resData = [];
      } else {
        let todoList = data.map((d: ITodoItem) => ({
          id: d._id,
          title: d.title,
          content: d.content,
          isDone: d.isDone,
          isPin: d.isPin,
        }));
        resData = todoList;
      }
    })
    return resData;
  }
}

export default new TodoItemController();